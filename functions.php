<?php

include_once 'assets/php/admin/settings.php';

/* ##### REGISTER MENUS ##### */
function titan_register_menus() {
	register_nav_menus(
		array(
			'header-menu' => __('Header Menu'),
		)
	);
}

class Titan_Walker_Nav_Menu extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\">\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$has_children = $args->walker->has_children && $depth < $args->depth - 1;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
   
		$class_names = $value = '';
   
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
   
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="asdf ' . esc_attr( $class_names ) . '"';
   
		$output .= $indent . '<li' . ($has_children ? ' class="dropdown"' : '') . '>';
   
		$attributes = $has_children ? ' class="dropdown-toggle"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
		$attributes .= $has_children ? ' data-toggle="dropdown"' : '';
   
		$item_output .= '<a'. $attributes .'>';
		$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
		$item_output .= '</a>';
   
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

add_action( 'init', 'titan_register_menus' );

/**
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

/**
 * Enable custom logos
 */
function titan_custom_logo_setup() {
    $defaults = array(
        'height'      => 150,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'titan_custom_logo_setup' );

/**
 * Enable customization of the theme
 */
const nav_bg_color_default = 'rgba(10, 10, 10, 0.9)';

function titan_customize_register( $wp_customize ) {
	//All our sections, settings, and controls will be added here
	$wp_customize->add_setting( 'titan_nav_bg_color', array(
		'default'	=> $nav_bg_color_default,
	));

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
		'label'		=> __( 'Navigation Background Color', 'titan' ),
		'section'	=> 'colors',
		'settings'	=> 'titan_nav_bg_color',
	)));
 }
 add_action( 'customize_register', 'titan_customize_register' );

 function titan_customize_css() {
	?>
	<style type="text/css">
		.navbar-custom:not(.navbar-transparent) {
			background-color: <?php echo get_theme_mod( 'titan_nav_bg_color', $nav_bg_color_default ); ?>;
		}
		@media (max-width: 767px) {
			.navbar-custom {
				background-color: <?php echo get_theme_mod( 'titan_nav_bg_color', $nav_bg_color_default ); ?>;
			}
		}
	</style>
	<?php
 }
 add_action( 'wp_head', 'titan_customize_css' );

/**
 * Enqueue scripts and styles.
 */
function titan_scripts() {
	/*
	*/
	// Add Bootstrap default CSS
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/lib/bootstrap/dist/css/bootstrap.min.css' );

	// Add Font Awesome stylesheet
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/lib/components-font-awesome/css/font-awesome.min.css' );

	// Add Google Fonts
    /*<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">*/
	wp_enqueue_style( 'titan-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:400,700%7CVolkhov:400i%7COpen+Sans:300,400,600,700,800' );

	// Add slider CSS
	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/assets/lib/flexslider/flexslider.css' );

	// Add animate CSS
	//<link href="assets/lib/animate.css/animate.css" rel="stylesheet">
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/lib/animate.css/animate.css' );

	// Add line font CSS
	//<link href="assets/lib/et-line-font/et-line-font.css" rel="stylesheet">
	wp_enqueue_style( 'et-line-font', get_template_directory_uri() . '/assets/lib/et-line-font/et-line-font.css' );

	// Add owl carousel CSS
	//<link href="assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/lib/owl.carousel/dist/assets/owl.carousel.min.css' );

	// Add owl theme CSS
	//<link href="assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
	wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css' );
	
	// Add magnific popup CSS
	//<link href="assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/lib/magnific-popup/dist/magnific-popup.css' );
	
	// Add simplex text rotator CSS
    //<link href="assets/lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">
	wp_enqueue_style( 'simplex-text-rotator', get_template_directory_uri() . '/assets/lib/simple-text-rotator/simpletextrotator.css' );

	//Add custom theme CSS
	wp_enqueue_style( 'titan-style', get_stylesheet_uri() );

	// Add custom color theme CSS
	wp_enqueue_style( 'titan-color-scheme', get_template_directory_uri() . '/assets/css/colors/default.css' );

	/**
	 * Include Javascript
	 */

	/*if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( post_type_exists( 'jetpack-portfolio' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}*/

	// Add bootstrap JS
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/lib/bootstrap/dist/js/bootstrap.min.js', array( 'jquery' ), false, true );

	// Add wow JS
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/assets/lib/wow/dist/wow.js', array( 'jquery' ), false, true );
	
	// Add jQuery MB YTPlayer JS
	wp_enqueue_script( 'jquery-mb-ytplayer', get_template_directory_uri() . '/assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js', array( 'jquery' ), false, true );

	// Add isotope JS
	wp_enqueue_script( 'isotope', get_template_directory_uri() . '/assets/lib/isotope/dist/isotope.pkgd.js', array( 'jquery' ), false, true );

	// Add imagesloaded JS
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/assets/lib/imagesloaded/imagesloaded.pkgd.js', array( 'jquery' ), false, true );

	// Add flexslider JS
	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/assets/lib/flexslider/jquery.flexslider.js', array( 'jquery' ), false, true );

	// Add owl carousel JS
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/lib/owl.carousel/dist/owl.carousel.min.js', array( 'jquery' ), false, true );

	// Add smoothscroll JS
	wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/assets/lib/smoothscroll.js', array( 'jquery' ), false, true );

	// Add magnific popup JS
	wp_enqueue_script( 'magific-popup', get_template_directory_uri() . '/assets/lib/magnific-popup/dist/jquery.magnific-popup.js', array( 'jquery' ), false, true );

	// Add simple text rotator JS
	wp_enqueue_script( 'simple-text-rotator', get_template_directory_uri() . '/assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js', array( 'jquery' ), false, true );

	// Add plugins JS
	wp_enqueue_script( 'titan-plugins', get_template_directory_uri() . '/assets/js/plugins.js', array( 'jquery' ), false, true );

	// Add main JS
	wp_enqueue_script( 'titan-main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'titan_scripts' );
