<?php

// Add the admin menu for adjusting the template
add_action( 'admin_menu', 'titan_add_menu_pages' );
function titan_add_menu_pages() {
	$page_title = 'General';
	$menu_title = 'Titan';
	$capabilities = 'manage_options';
	$menu_slug = 'titan_general';
	$function = 'titan_draw_general_settings';
	$icon_url = '';
	$position = 99;

	// Add a menu for the Titan settings
	add_menu_page( $page_title, $menu_title, $capabilities, $menu_slug, $function, $icon_url, $position );

	// Add the general settings page (same slug as the menu, such that the first submenu item is called
	// differently then the menu itself)
	add_submenu_page( $menu_slug, $page_title, $page_title, $capabilities, $menu_slug, $function );
}

// Draw the general settings page
function titan_draw_general_settings() {
	include 'general-settings-form.php';
}

/* Add general settings */
/*function titan_add_general_settings() {
	// Add a section to the general settings tab
	add_settings_section( 
		'titan_general_settings_section', 
		'Titan Settings', 
		'titan_general_settings_section_callback', 
		'general'
	);

	// Add general settings fields
	add_settings_field( 
		'titan_description_setting', 
		'Description', 
		'titan_description_setting_callback', 
		'general', 
		'titan_general_settings_section'
	);

	// Register the setting
	register_setting( 'general', 'titan_description_setting' );
}

add_action( 'admin_init', 'titan_add_settings' );*/

/* ##### SETTINGS SECTION CALLBACK FUNCTIONS ##### */
/*function titan_general_settings_section_callback() {
}*/

/* ##### SETTINGS SECTION CALLBACK FUNCTIONS ##### */
/*function titan_description_setting_callback() {
	echo '<textarea name="titan_description_setting" id="titan_description_setting" rows="8" cols="120">' . get_option( 'titan_description_setting' ) . '</textarea>';
}*/