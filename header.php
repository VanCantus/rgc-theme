<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>

    <!--<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo( 'template_url' ); ?>/assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">-->
  </head>

  <body <?php body_class(); ?> data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <!-- TODO: Add loader for page (load the basic content like header and page structure without content via REST API) -->
      <div class="page-loader">
        <div class="loader">Loading...</div>
      </div>
      <nav class="navbar navbar-custom navbar-fixed-top<?php if (is_single()): echo " navbar-transparent"; endif; ?>" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <?php
            if ( function_exists( 'the_custom_logo' ) ) {
              the_custom_logo();
            }
            ?>
          </div>
          <?php
          // Add navigation bar
          $args = array(
            'menu_class' => 'nav navbar-nav navbar-right',
            'container' => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'custom-collapse',
            'fallback_cb' => true,
            'echo' => true,
            'depth' => 3,
            'theme_location' => 'header-menu',
            'walker' => new Titan_Walker_Nav_Menu(),
          );

          wp_nav_menu( $args );
          ?>
        </div>
      </nav>

      <!-- Slider on start page (only visible at the front page!) -->
      <?php if ( is_front_page( ) ): ?>
      <section class="home-section home-parallax home-fade home-full-height" id="home">
        <div class="hero-slider">
          <ul class="slides">
            <li class="bg-dark-30 bg-dark" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/assets/images/section-8.jpg);">
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-1">Hello &amp; welcome</div>
                  <div class="font-alt mb-40 titan-title-size-4">We are Titan</div><a class="section-scroll btn btn-border-w btn-round" href="#about">Learn More</a>
                </div>
              </div>
            </li>
            <li class="bg-dark-30 bg-dark" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/assets/images/section-9.jpg);">
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-2">Titan is creative multipurpose html template for<br/>web developers who change the world
                  </div><a class="btn btn-border-w btn-round" href="about">Learn More</a>
                </div>
              </div>
            </li>
            <li class="bg-dark-30 bg-dark" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/assets/images/section-10.jpg);">
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-1">We build brands that build business</div>
                  <div class="font-alt mb-40 titan-title-size-3">We are Amazing</div><a class="section-scroll btn btn-border-w btn-round" href="#about">Learn More</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </section>
      <?php endif; ?>